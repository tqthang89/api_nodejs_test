const express = require('express');
const router = express.Router();

const openai = require('../libs/openai');
const Speech = require('../libs/speech');
const Conversation = require('../libs/conversation');

const { Configuration, OpenAIApi } = require("openai");

router.get('/', function (req, res) {
    res.send(`this is chat page`);
})

router.post('/message', async function (req, res) {
    const { token, id, prompt } = req.body;
    if (token) {
        openai.setToken(token);
    }

    console.log("done settoken");

    const speech = new Speech();

    const chat = new Conversation(id);
    chat.setMessage('question', prompt);

    console.log("setMessage");

    const resp = await openai.completions({ prompt: chat.makePromt() });
    //const resp = await openai.completions({ prompt: prompt + "\n\n" });
    console.log(resp);

    if (resp && resp.choices) {
        const answer = resp.choices[0].text;
        const voice = speech.makeVoice(answer);
        const voices = speech.makeVoices(answer);
        chat.setMessage('answer', answer);
        return res.send({url: url.parse(req.url), id: chat._id, message: chat.lastMessage, voice, voices, items: chat._data.items });
    }


    /////////////WIn//////////////////////////////////
    // const configuration = new Configuration({
    //     apiKey:token// process.env.OPENAI_API_KEY,
    //   });
    //   const openai = new OpenAIApi(configuration);
    // const response = await openai.createCompletion({
    //     model: "text-davinci-003",
    //     prompt: prompt,
    //     temperature: 0,
    //     max_tokens: 100,
    //     top_p: 1,
    //     frequency_penalty: 0.0,
    //     presence_penalty: 0.0,
    //     stop: ["\n"],
    //   });

    //   console.log(response);

    //    if (response && response.choices) {
    //     const answer = response.choices[0].text;
    //     const voice = speech.makeVoice(answer);
    //     const voices = speech.makeVoices(answer);
    //     chat.setMessage('answer', answer);
    //     return res.send({ id: chat._id, message: chat.lastMessage, voice, voices, items: chat._data.items });
    // }

    ////////////////////////////////

    res.send(`error send message`);
})

module.exports = router;
