# exits in docker hub
FROM node:18-alpine

#RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app

WORKDIR /

COPY . .

#USER node

RUN npm install

#COPY --chown=node:node . .
# # 
EXPOSE 3000

CMD [ "node", "app.js" ]

